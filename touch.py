#!/usr/bin/env python3
import codecs
import time
import math
import glob
import pyudev
import os
from pymouse import PyMouse
import re

MAX_X = 4000  # Is the maximum value of the X coord returned by the touch screen
MAX_Y = 4000  # Is the maximum value of the Y coord returned by the touch screen

# Dimensions of the screen
S_SIZE_X = 800
S_SIZE_Y = 480

'''
This regex should match:
    "aa" -> begin of command
    "01" -> clicked
    [intHEX] -> X coordinates
    [intHEX] -> Y coordinates
'''
regex = r"(aa01)([0-9a-fA-F]{4})([0-9a-fA-F]{4})"

# Wait and find devices
def read_and_emulate_mouse(deviceFound):
    with open(deviceFound, 'rb') as f:
        m = PyMouse()  # Mouse object
        clicked = False
        rightClicked = False
        (lastX, lastY) = (0, 0)
        startTime = time.time()

        for chunk in iter(lambda: f.read(32), b''):  # Getting 32 bytes of the data sent by the HID touch screen
            data = (codecs.encode(chunk, 'hex')).decode("utf-8")  # Decode the bytes in a str type containing hex data

            matches = re.finditer(regex, data)  # Find in the hex data something matching our regex
             
            for matchNum, match in enumerate(matches):
                matchNum = matchNum + 1
                x = int(match.group(2), 16)  # Getting the X coor in HEX and convert it in decimal
                y = int(match.group(3), 16)  # Getting the Y coor in HEX and convert it in decimal

            '''
            Calcul method for converting the huge values returned by the screen in a position for the mouse:
            Indeed the screen HID serial does not return a value in the range of the screen size, the value are ranged
            between 0 and 4000.
            To convert it to a position for the mouse we create a ratio for the finger position by dividing the x value
            by the maximum x values returned by the screen (4000). Then we multiply this ratio by the screen size.'''
            realX = int((x/MAX_X)*S_SIZE_X)
            realY = int((y/MAX_Y)*S_SIZE_Y)

            m.click(realX, realY, 1)  # Moving the mouse to the finger coordinates


if __name__ == "__main__":
    os.system("sudo modprobe uinput")
    os.system("sudo chmod 666 /dev/hidraw*")
    os.system("sudo chmod 666 /dev/uinput*")

    while True:
        # try:
        print("Waiting device")
        hidrawDevices = glob.glob("/dev/hidraw*")
        print(hidrawDevices)
        context = pyudev.Context()
        print(context)
        deviceFound = None
        for hid in hidrawDevices:
            device = pyudev.Device.from_device_file(context, hid)
            if "0EEF:0005" in device.device_path:
                deviceFound = hid

        if deviceFound:
            print("Device found", deviceFound)
            read_and_emulate_mouse(deviceFound)
            # except:
            #     print("Error:", sys.exc_info())
            #     pass
            # finally:
            #     time.sleep(1)
